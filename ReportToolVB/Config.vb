﻿Module Config
    '全局变量
    Public C_LOGIN_URL, C_LOGIN_ACCOUNT, C_LOGIN_PASSWORD As String
    Public C_REPORT_URL, C_REPORT_DATE, C_REPORT_TOPIC, C_REPORT_CONTENT1, C_REPORT_CONTENT2, C_REPORT_CONTENT3, C_REPORT_CONTENT4, C_REPORT_CONTENT5 As String

    '程序启动时初始化所有配置,如果找不到配置文件/参数,则自动生成
    Public Sub InitConfiguration()

        '全局变量
        C_LOGIN_URL = System.Configuration.ConfigurationManager.AppSettings("LoginUrl") '登录地址
        C_LOGIN_ACCOUNT = System.Configuration.ConfigurationManager.AppSettings("Account") '登录帐号
        C_LOGIN_PASSWORD = System.Configuration.ConfigurationManager.AppSettings("Password") '登录密码

        C_REPORT_URL = System.Configuration.ConfigurationManager.AppSettings("ReportUrl") '工作报告地址
        C_REPORT_DATE = System.Configuration.ConfigurationManager.AppSettings("Date") '报告日期
        C_REPORT_TOPIC = System.Configuration.ConfigurationManager.AppSettings("Topic") '报告主题
        C_REPORT_CONTENT1 = System.Configuration.ConfigurationManager.AppSettings("Content1") '报告内容模板1
        C_REPORT_CONTENT2 = System.Configuration.ConfigurationManager.AppSettings("Content2") '报告内容模板2
        C_REPORT_CONTENT3 = System.Configuration.ConfigurationManager.AppSettings("Content3") '报告内容模板3
        C_REPORT_CONTENT4 = System.Configuration.ConfigurationManager.AppSettings("Content4") '报告内容模板4
        C_REPORT_CONTENT5 = System.Configuration.ConfigurationManager.AppSettings("Content5") '报告内容模板5

    End Sub

End Module
