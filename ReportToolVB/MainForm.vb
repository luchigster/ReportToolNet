﻿Public Class MainForm

    Public Sub WriteToForm(ByVal Obj As Object, ByVal sTagName As String, ByVal sName As String, ByVal Submit As Boolean, Optional ByVal sType As String = "", Optional ByVal sValue As String = "", Optional ByVal sSelect As Boolean = False)
        Dim i As Integer
        Dim vDoc, vTag
        sTagName = UCase(sTagName)
        sType = UCase(sType)
        sName = UCase(sName)
        vDoc = Obj.Document
        For i = 0 To vDoc.All.Length - 1
            If UCase(vDoc.All(i).tagname) = sTagName Then
                vTag = vDoc.All(i)
                If sTagName = "SELECT" Then
                    If UCase(vTag.Name) = sName Then vTag.value = sValue
                ElseIf sTagName = "TEXTAREA" Then
                    If UCase(vTag.Name) = sName Then vTag.value = vTag.value + sValue + Chr(10) + Chr(13)
                Else
                    If sType = "RADIO" Or sType = "CHECKBOX" Then
                        sValue = UCase(sValue)
                        If UCase(vTag.Name) = sName And UCase(vTag.value) = sValue Then
                            If sSelect Then
                                vTag.Checked = True
                            Else
                                vTag.Checked = False
                            End If
                        End If
                    Else
                        If Submit Then
                            If UCase(vTag.Type) = sType And UCase(vTag.Name) = sName Then vTag.Click()
                        Else
                            If UCase(vTag.Type) = sType And UCase(vTag.Name) = sName Then vTag.value = sValue
                        End If
                    End If
                End If
            End If
        Next i

    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call InitConfiguration()

        WebBrowser2.Navigate(C_LOGIN_URL)

        Combo1.Items.Clear()
        Combo1.Items.Add(C_REPORT_CONTENT1)
        Combo1.Items.Add(C_REPORT_CONTENT2)
        Combo1.Items.Add(C_REPORT_CONTENT3)
        Combo1.Items.Add(C_REPORT_CONTENT4)
        Combo1.Items.Add(C_REPORT_CONTENT5)
        Combo1.SelectedIndex = 0
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        WriteToForm(WebBrowser2, "input", "txt_Account", False, "text", C_LOGIN_ACCOUNT)     '输入帐号
        WriteToForm(WebBrowser2, "input", "txt_Password", False, "password", C_LOGIN_PASSWORD)     '输入密码
        WriteToForm(WebBrowser2, "input", "btn_SignIn", True, "image", "submit")     '提交表单

        Timer1.Start()
    End Sub

    Private Sub Timer1_Tick_1(sender As Object, e As EventArgs) Handles Timer1.Tick
        If WebBrowser2.ReadyState = SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE Then
            WebBrowser2.Navigate(C_REPORT_URL)
            Timer1.Stop()
            Timer2.Start()
        End If
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If WebBrowser2.ReadyState = SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE Then
            WriteToForm(WebBrowser2, "select", "dbl_SendDate_Day", False, "", C_REPORT_DATE)     '选择当天
            WriteToForm(WebBrowser2, "input", "txt_Subject", False, "text", C_REPORT_TOPIC)     '输入主题
            WriteToForm(WebBrowser2, "textarea", "txt_Content", False, "", Combo1.Text)     '输入工作内容
            Timer2.Stop()
        End If
    End Sub

    Private Sub Combo1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Combo1.SelectedIndexChanged
        WriteToForm(WebBrowser2, "textarea", "txt_Content", False, "", Combo1.Text)     '输入工作内容
    End Sub
End Class
